return function()
    local TestClass = require(script.Parent)

    describe("TestClass", function()
        describe("addNumbers", function()
            it("should add numbers", function()
                expect(TestClass.addNumbers(123, 456)).to.equal(123 + 456)
            end)
        end)
    end)
end